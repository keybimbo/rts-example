﻿using UnityEngine;
using System.Collections;

public class sprite_order : MonoBehaviour {

	SpriteRenderer renderer;

	void Start ()
	{
		renderer = gameObject.GetComponent<SpriteRenderer> ();

	}

	void LateUpdate () 
	{
		renderer.sortingOrder = (int)Camera.main.WorldToScreenPoint (renderer.bounds.min).y * -1;
	}
}
